﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreezyPredectionAPI.Interface
{
    interface IMakePredection
    {
        string predectionKey { get; set; }
        string predectionUrl { get; set; }

        string imageFilePath { get; set; }

    }
}
