﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace FreezyPredectionAPI
{
    class MakePredection
    {
        public static async Task MakePredictionRequest(string imageFilePath)
        {
            var client = new HttpClient();

            // Request headers - replace this example key with your valid Prediction-Key.
            client.DefaultRequestHeaders.Add("Prediction-Key", "9b80c717de304b53b3fe1523599d94f0");

            // Prediction URL - replace this example URL with your valid Prediction URL.

            //Image file
            string url = "https://southcentralus.api.cognitive.microsoft.com/customvision/v3.0/Prediction/f729c60f-3b94-4984-8c0f-bff61f65b8a3/detect/iterations/Iteration7/image";

            //Image URL
            //https://southcentralus.api.cognitive.microsoft.com/customvision/v3.0/Prediction/f729c60f-3b94-4984-8c0f-bff61f65b8a3/detect/iterations/Iteration7/url

            HttpResponseMessage response;

            // Request body. Try this sample with a locally stored image.
            byte[] byteData = ImageToByteArray.GetImageAsByteArray(imageFilePath);

            using (var content = new ByteArrayContent(byteData))
            {
                content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                response = await client.PostAsync(url, content);

                Console.WriteLine(await response.Content.ReadAsStringAsync());
            }
        }
    }
}
