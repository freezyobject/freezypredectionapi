﻿using System;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace FreezyPredectionAPI
{
    public static class Program
    {
        public static void Main()
        {

            //Get Image Path and Predection request
            MakePredection.MakePredictionRequest(GetImage.ImagePath()).Wait();

            Console.WriteLine("\n\nHit ENTER to exit...");
            Console.ReadLine();
        }        

        
    }
}